import React from 'react';
import { ThemeContext } from '../contexts/ThemeContext';
import { BookContext } from '../contexts/BookContext';
import ThemeToggle from './ThemeToggle';

const BookList = () => {
    const {books, setBooks} = React.useContext(BookContext);
    const {isLightTheme, light, dark } = React.useContext(ThemeContext);
    const theme = isLightTheme ? light : dark;
    return (
        <div className="book-list" style={{color: theme.font, background: theme.bg}}>
            <ul>
                {books.map(book => {
                    return (
                    <li style={{color: theme.font, background: theme.ui}} key={book.id}>{book.title}</li>
                    )
                })}
            </ul>
            <hr/>
            <ThemeToggle />
        </div>
    );
}

export default BookList;