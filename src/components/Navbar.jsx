import React from 'react';
import { ThemeContext } from '../contexts/ThemeContext';
import { AuthContext } from '../contexts/AuthContext';

const Navbar = () => {
    const { isAuthenticated, toggleAuth } = React.useContext(AuthContext);
    const { isLightTheme, light, dark } = React.useContext(ThemeContext);
    const theme = isLightTheme ? light : dark;

    return (        
        <nav style={{color: theme.font, background: theme.ui}}>
            <h1>Context App</h1>
            <div onClick={toggleAuth}>
                { isAuthenticated ? "Logged In" : "Logged Out" }
            </div>
            <ul>
                <li>Home</li>
                <li>About</li>
                <li>Contact</li>
            </ul>
        </nav>
    );
}

export default Navbar;