import React from 'react';
import { ThemeContext } from '../contexts/ThemeContext';

const ThemeToggle =() => {
    const { toggleTheme, isLightTheme, light, dark } = React.useContext(ThemeContext);
    const theme = isLightTheme ? light : dark;
    return (
        <button onClick={ toggleTheme } style={{color: theme.font, background : theme.ui}}>Toggle the theme</button>
    )
} 

export default ThemeToggle;